# SLAMXS-Project

Project of Slam for Soc Systems

## Getting Started 

1. Clone the repo
   ```sh
   git clone https://gitlab.com/gabrieldiaz94/slamxs-project.git
   ```
2. go to slam folder
   ```sh
   cd slamxs-project
   ```
4. Build your image 
   ```
   source build.sh 
   ```
5. Run your image
   ```
   source run.sh 
   ```
6. Inside docker run and yes to the terms and conditions:
   ```
   source init.bash 
   ```
7. When you have rviz include the point cloud of the topics created.
