FROM osrf/ros:kinetic-desktop-full

RUN apt-get update && apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 4B63CF8FDE49746E98FA01DDAD19BAB3CBF125EA && \
    echo "deb http://snapshots.ros.org/kinetic/final/ubuntu xenial main" > /etc/apt/sources.list.d/ros1-snapshots.list && \
    apt-get install -y ros-kinetic-turtlebot && \
    apt-get install -y ros-kinetic-turtlebot-.* && \
    apt-get install -y nano

RUN useradd -m -s /bin/bash -N -u 1000 slamxs && \
    echo "pepper ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers && \
    chmod 0440 /etc/sudoers && \
    chmod g+w /etc/passwd 

USER slamxs

WORKDIR home/slamxs/app

COPY --chown=slamxs app/ .


RUN chmod 774 ~/app/slamxs_ws/

RUN /bin/bash -c "source /opt/ros/kinetic/setup.bash;"

