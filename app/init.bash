#!/bin/bash

cd slamxs_ws
catkin_make
source devel/setup.bash
export ROS_HOME=~/app/slamxs_ws
roslaunch slamxs slam.launch

