#include "simplePoint.h"
#include <vector>

#pragma once

namespace slamxs{

    class PointCloudSlamxs {
        public:
            PointCloudSlamxs() = default;
            PointCloudSlamxs(const double x, const double y, const double z);
            PointCloudSlamxs(const SimplePoint &point);
            ~PointCloudSlamxs() { }

            void setPoint(const SimplePoint& point);
            std::vector<SimplePoint> &pointcloud();

            void limitPoints();
            std::vector<SimplePoint> pointsOrganized(int indexCuadrant);

        private:
            void splitByCuadrant(std::vector<SimplePoint> &firstCuadrant, std::vector<SimplePoint> &secondCuadrant, std::vector<SimplePoint> &thirdCuadrant, std::vector<SimplePoint> &fourCuadrant);

            std::vector<SimplePoint> _pointcloud;
            std::vector<SimplePoint>::iterator it_;
    };
}