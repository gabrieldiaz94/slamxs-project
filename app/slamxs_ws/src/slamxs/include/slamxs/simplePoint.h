#include <array>
#pragma once

namespace slamxs{

    class SimplePoint{
        public:
            SimplePoint() = default;
            SimplePoint(const double x, const double y, const double z);
            SimplePoint(std::initializer_list <double> list);
            ~SimplePoint() { }

            //Operator
            bool operator <=(const SimplePoint &rhs) const;
            bool operator >(const SimplePoint &rhs) const;
            bool operator == (const SimplePoint &rhs) const;

            double &x();
            double &y();
            double &z();
        private:
            double x_;
            double y_;
            double z_;
    };
}