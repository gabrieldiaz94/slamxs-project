#include "slamxs/simplePoint.h"
#include <ros/ros.h>
#include <gtest/gtest.h>
#include <thread>
#include <chrono>

class SimplePointTest: public ::testing::Test {
    public:
    SimplePointTest() = default;

    ~SimplePointTest(){ }
};


TEST_F(SimplePointTest, Initializer) {
    slamxs::SimplePoint p{1., 2., 3.};
    EXPECT_EQ(p.x(), 1.);
    EXPECT_EQ(p.y(), 2.);
    EXPECT_EQ(p.z(), 3.); 
}

TEST_F(SimplePointTest, GreaterThan) {
    slamxs::SimplePoint p{1., 2., 3.};
    const slamxs::SimplePoint p1{0.1, 0.2, 0.3};
    EXPECT_TRUE(p > p1); 
}

TEST_F(SimplePointTest, LowerEqualThan) {
    slamxs::SimplePoint p{1., 2., 3.};
    const slamxs::SimplePoint p1{10., 20., 30.};
    EXPECT_TRUE(p <= p1); 
}