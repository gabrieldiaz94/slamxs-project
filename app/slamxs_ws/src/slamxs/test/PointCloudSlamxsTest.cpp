#include "slamxs/PointCloudSlamxs.h"
#include <ros/ros.h>
#include <gtest/gtest.h>

class PointCloudSlamxs: public ::testing::Test {
    public:
    PointCloudSlamxs() = default;

    ~PointCloudSlamxs(){ }
};


TEST_F(PointCloudSlamxs, Initializer) {
    const slamxs::SimplePoint p1{0.1, 0.2, 0.3};
    const slamxs::SimplePoint p2{1.0, 2.0, 3.0};
    const slamxs::SimplePoint p3{10.0, 20.0, 30.0};
    const slamxs::SimplePoint p4{100.0, 200.0, 300.0};
    const std::vector<slamxs::SimplePoint> expectedPointCloud{p1, p2, p3, p4};
    const std::vector<slamxs::SimplePoint> expectedP1{p1};
    slamxs::PointCloudSlamxs points;
    slamxs::PointCloudSlamxs points1{p1};
    slamxs::PointCloudSlamxs points2;
    points.setPoint(p1);
    points.setPoint(p2);
    points.setPoint(p3);
    points.setPoint(p4);


    EXPECT_EQ(points.pointcloud(),expectedPointCloud);
    EXPECT_EQ(points.pointcloud().size(), 4);
    EXPECT_EQ(points1.pointcloud(),expectedP1);
    EXPECT_EQ(points1.pointcloud().size(), 1);
    EXPECT_TRUE(points2.pointcloud().empty());

}

TEST_F(PointCloudSlamxs, LimitPoint) {
    const slamxs::SimplePoint p1{0.1, 0.2, 0.3};
    const slamxs::SimplePoint p2{1.0, 2.0, 3.0};
    const slamxs::SimplePoint p3{10.0, 20.0, 30.0};
    const slamxs::SimplePoint p4{100.0, 200.0, 300.0};
    const std::vector<slamxs::SimplePoint> expectedPointCloud{p1};
    slamxs::PointCloudSlamxs points;
    points.setPoint(p1);
    points.setPoint(p2);
    points.setPoint(p3);
    points.setPoint(p4);
    points.limitPoints();


    EXPECT_EQ(points.pointcloud(),expectedPointCloud);
}
