#include <gtest/gtest.h>
#include <ros/ros.h>
#include <thread>
#include <chrono>

int main(int argc, char** argv) {
    ros::init(argc, argv, "PointCloudTests");
    
    testing::InitGoogleTest(&argc, argv);
    
    std::thread t([]{while(ros::ok()) ros::spin();});
    
    auto res = RUN_ALL_TESTS();
    
    ros::shutdown();
    
    return res;
}