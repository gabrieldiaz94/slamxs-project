#include <iostream>
#include <ros/ros.h>
#include <string>   
#include <stdlib.h>
#include <stdio.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/io/pcd_io.h>
#include <fstream>
#include <math.h>
#include <nav_msgs/Odometry.h>
#define PI 3.14159265
struct Puntos{
 double  x, y,z;
 int n,val,cuadranteC,cuadranteE;
};
struct Voxel{
 double valores [21845][3];
 int cantidad;
}Cubo,Cubo2;
struct slamc{
    double movimientocarro[32767][2];
    int maximosCoord[8191][4];
    //int maximosCoord1[8191][2];
    //int maximosCoord2[8191][2];
   // int maximosCoord3[8191][2];
  //  int maximosCoord4[8191][2];
    int cantidad;
}Carro;
nav_msgs::Odometry odome;
Puntos* limites(Puntos* punto,int n,int maxX,int minX,int maxY, int minY, int maxZ, int minZ){
    Puntos* limits=new Puntos[n];
    int ii=0;
    for (int i=0;i<n;i++){
        if((punto[i].x>minX && punto[i].x<=maxX) && (punto[i].y>minY && punto[i].y<=maxY) && (punto[i].z>minZ && punto[i].z<=maxZ)){
            limits[ii].x=punto[i].x;
            limits[ii].y=punto[i].y;
            limits[ii].z=punto[i].z;
            limits[ii].val=0;
            ii=ii+1;
        }
    }
    limits[0].n=ii;
    return limits;
}

 int** grupos(int n, int m, int puntos1[][3],int puntos2[][3]){
    int** vectores = 0;
    vectores = new int*[n+m];
    for(int i=0;i<n+m;i++){
        vectores[i] = new int[3];
        if(i<n){
            vectores[i][0]=puntos1[i][0];
            vectores[i][1]=puntos1[i][1];
            vectores[i][2]=puntos1[i][2];
        }
        if( i-n>0 && i-n<m){
            vectores[i][0]=puntos2[i-n][0];
            vectores[i][1]=puntos2[i-n][1];
            vectores[i][2]=puntos2[i-n][2];
        }
    }
    return vectores;
}
int** gruposorg(int n,int puntos1[][3],int m,int ycuad,int xcuad,int zcuad){
    int** vectores = 0;
    int nuevofrente[n][7],nuevofrente1[n][4],nuevofrentex[n][50],nuevofrentey[n][50],nuevofrentez[n][50];
    int vary,varx,varz,varz1,varz2;
    float r,r1,r2,xx1,yy1,xx2,yy2;
    float diffz1,diffz2,diffz3,diffz4,diffz5,theta,theta2,theta3;
    int ncfrente=0;
    int aux;
    double anglesr= (PI/4)*m;
    for(int i=0;i<n;i++){
        varz=puntos1[i][2]/zcuad;
        varz=varz*zcuad;
        varz1=varz+zcuad;
        varx=puntos1[i][0];
        varx=varx/xcuad;
        varx=varx*xcuad;
        vary=puntos1[i][1];
        vary=vary/ycuad;
        vary=vary*ycuad;
        if(ncfrente>0){
            aux=0;
            for(int j=0;j<ncfrente;j++){
                r1=sqrt(pow(nuevofrente[j][0]-varx,2)+pow(nuevofrente[j][1]-vary,2));
                r=sqrt(pow(xcuad,2)+pow(ycuad,2));
                if(r1<=r && varz==nuevofrente[j][2]){
                    aux=1;
                    nuevofrente[j][3]=nuevofrente[j][3]+1;
                    nuevofrentex[j][nuevofrentex[j][0]]=puntos1[i][0];
                    nuevofrentey[j][nuevofrentey[j][0]]=puntos1[i][1];
                    nuevofrentez[j][nuevofrentez[j][0]]=varz;
                    nuevofrentex[j][0]=nuevofrentex[j][0]+1;
                    nuevofrentey[j][0]=nuevofrentey[j][0]+1;
                    nuevofrentez[j][0]=nuevofrentez[j][0]+1;
                    //OPcion1
              /*      nuevofrente[j][4]=nuevofrente[j][4]+varx;
                    nuevofrente[j][5]=nuevofrente[j][5]+vary;
                    nuevofrente[j][6]=nuevofrente[j][6]+varz;*/
                    //OPcion 2
                    nuevofrente[j][4]=nuevofrente[j][4]+puntos1[i][0];
                    nuevofrente[j][5]=nuevofrente[j][5]+puntos1[i][1];
                    nuevofrente[j][6]=nuevofrente[j][6]+varz;
                }
            }
            if(aux==0){
                nuevofrente[ncfrente][0]=varx;
                nuevofrente[ncfrente][1]=vary;
                nuevofrente[ncfrente][2]=varz;
                nuevofrente[ncfrente][3]=1;
                 // Opcion 1
               /* nuevofrente[ncfrente][4]=varx;
                nuevofrente[ncfrente][5]=vary;
                nuevofrente[ncfrente][6]=varz;*/
                // Opcion 2
                nuevofrente[ncfrente][4]=puntos1[i][0];
                nuevofrente[ncfrente][5]=puntos1[i][1];
                nuevofrente[ncfrente][6]=varz;
                nuevofrentex[ncfrente][0]=2;
                nuevofrentey[ncfrente][0]=2;
                nuevofrentez[ncfrente][0]=2;
                nuevofrentex[ncfrente][1]=puntos1[i][0];
                nuevofrentey[ncfrente][1]=puntos1[i][1];
                nuevofrentez[ncfrente][1]=varz;       
                ncfrente=ncfrente+1;
            }
        }else{
                nuevofrente[ncfrente][0]=varx;
                nuevofrente[ncfrente][1]=vary;
                nuevofrente[ncfrente][2]=varz;
                nuevofrente[ncfrente][3]=1;
                // Opcion 1
            /*    nuevofrente[ncfrente][4]=varx;
                nuevofrente[ncfrente][5]=vary;
                nuevofrente[ncfrente][6]=varz;*/
                // Opcion 2
                nuevofrente[ncfrente][4]=puntos1[i][0];
                nuevofrente[ncfrente][5]=puntos1[i][1];
                nuevofrente[ncfrente][6]=varz;
                nuevofrentex[ncfrente][0]=2;
                nuevofrentey[ncfrente][0]=2;
                nuevofrentez[ncfrente][0]=2;
                nuevofrentex[ncfrente][1]=puntos1[i][0];
                nuevofrentey[ncfrente][1]=puntos1[i][1];
                nuevofrentez[ncfrente][1]=varz;
                ncfrente=ncfrente+1;
            }
    }
    int xx,yy,zz;
    for(int i=0;i<ncfrente;i++){
        nuevofrente[i][0]=nuevofrente[i][4]/nuevofrente[i][3]; 
        nuevofrente[i][1]=nuevofrente[i][5]/nuevofrente[i][3];
        nuevofrente[i][2]=nuevofrente[i][6]/nuevofrente[i][3];
    }
    vectores = new int*[ncfrente+1];
    for(int i=0;i<ncfrente+1;i++){
        vectores[i] = new int[3];
        if(i==0){
            vectores[i][0]=ncfrente+1;
            vectores[i][1]=ncfrente+1;
            vectores[i][2]=ncfrente+1;
        }else{
            vectores[i][0]=nuevofrente[i-1][0];
            vectores[i][1]=nuevofrente[i-1][1];
            vectores[i][2]=nuevofrente[i-1][2];
        }
    }
    return vectores;
}
Puntos* organizar(Puntos* puntos, int n,double x, double y, double z){
    int xx,yy,zz,cuad1=0,cuad2=0,cuad3=0,cuad4=0;
    int xcuad=x*1000,ycuad=y*1000,zcuad=z*1000;
    int cuadrante1[n][3],cuadrante2[n][3],cuadrante3[n][3],cuadrante4[n][3];
    for(int i=0;i<n;i++){
        xx=puntos[i].x*1000;
        yy=puntos[i].y*1000;
        zz=puntos[i].z*1000;
        if(zz>-380){
            if(xx>=0 && yy>=0){
                cuadrante1[cuad1][0]=xx;
                cuadrante1[cuad1][1]=yy;
                cuadrante1[cuad1][2]=zz;
                cuad1=cuad1+1;
            }
            if(xx<0 && yy>=0){
                cuadrante2[cuad2][0]=xx;
                cuadrante2[cuad2][1]=yy;
                cuadrante2[cuad2][2]=zz;
                cuad2=cuad2+1;
            }
            if(xx<0 && yy<0){
                cuadrante3[cuad3][0]=xx;
                cuadrante3[cuad3][1]=yy;
                cuadrante3[cuad3][2]=zz;
                cuad3=cuad3+1;
            }
            if(xx>=0 && yy<0){
                cuadrante4[cuad4][0]=xx;
                cuadrante4[cuad4][1]=yy;
                cuadrante4[cuad4][2]=zz;
                cuad4=cuad4+1;
            }
        }
    }
 //   int** frente1= grupos(contar02,contar11,coord02,coord11);
    int anglec=1;
    int** nuevofrente1=gruposorg(cuad1,cuadrante1,anglec,ycuad,xcuad,zcuad);
    anglec=-1;
    int** nuevofrente2=gruposorg(cuad2,cuadrante2,anglec,ycuad,xcuad,zcuad);
    anglec=1;
    int** nuevofrente3=gruposorg(cuad3,cuadrante3,anglec,ycuad,xcuad,zcuad);
    anglec=-1;
    int** nuevofrente4=gruposorg(cuad4,cuadrante4,anglec,ycuad,xcuad,zcuad);
    std::cout<<"hoal"<<std::endl;
    Puntos* puntosV=new Puntos[nuevofrente1[0][0]+nuevofrente2[0][0]+nuevofrente3[0][0]+nuevofrente4[0][0]];
    int iis=0;
    for(int i=1;i<nuevofrente1[0][0];i++){
                    puntosV[iis].x=(double)  nuevofrente1[i][0]/1000;
                    puntosV[iis].y= (double) nuevofrente1[i][1]/1000;
                    puntosV[iis].z= (double) nuevofrente1[i][2]/1000;
                    iis=iis+1;
    }
    for(int i=1;i<nuevofrente2[0][0];i++){
                    puntosV[iis].x=(double)  nuevofrente2[i][0]/1000;
                    puntosV[iis].y= (double) nuevofrente2[i][1]/1000;
                    puntosV[iis].z= (double) nuevofrente2[i][2]/1000;
                    iis=iis+1;
    }
    for(int i=1;i<nuevofrente3[0][0];i++){
                    puntosV[iis].x=(double)  nuevofrente3[i][0]/1000;
                    puntosV[iis].y= (double) nuevofrente3[i][1]/1000;
                    puntosV[iis].z= (double) nuevofrente3[i][2]/1000;
                    iis=iis+1;
    }
    for(int i=1;i<nuevofrente4[0][0];i++){
                    puntosV[iis].x=(double)  nuevofrente4[i][0]/1000;
                    puntosV[iis].y= (double) nuevofrente4[i][1]/1000;
                    puntosV[iis].z= (double) nuevofrente4[i][2]/1000;
                    iis=iis+1;
    }
    puntosV[0].n=iis;
    return  puntosV;
}
int carrito=0;
int carrito1=0;
sensor_msgs::PointCloud2 entrada;
void cloudCB(const sensor_msgs::PointCloud2 &input)
{ 
    entrada=input;
    carrito=1;    
}
double posicionx,posiciony;
double qz,qw,s;
void odometria(const nav_msgs::Odometry &input){
    double posesx=(input.pose.pose.position.x)*100;
    double posesy=(input.pose.pose.position.y)*100;
    posicionx=input.pose.pose.position.x;
    posiciony=input.pose.pose.position.y;
    qz=input.pose.pose.orientation.z;
    qw=input.pose.pose.orientation.w;
    s=pow(sqrt(pow(qz,2)+pow(qw,2)),-2);
    if(carrito1==0){
        carrito1=1;
    }  
}
int cambio;
void llenarmapa(Puntos* puntos, int n,int posicionesx,int posicionesy){
    int puntosmov[n][3];
    for(int i=0;i<n;i++){
        if(puntos[i].z>-0.4){
            puntosmov[i][0]=(puntos[i].x*100)+posicionesx;
            puntosmov[i][1]=(puntos[i].y*100)+posicionesy;
            puntosmov[i][2]=(puntos[i].z*100);
        }
    }

}
main (int argc, char **argv)
{
	ros::init (argc, argv, "read_data");
	ros::NodeHandle nh;
//	ros::Subscriber bat_sub=nh.subscribe("/rslidar_points",64,cloudCB);
    ros::Subscriber bat_sub=nh.subscribe("/rs_points",64,cloudCB);
    ros::Subscriber bat_sub1=nh.subscribe("/odom",64,odometria);
    ros::Publisher pcl_pub2=nh.advertise<sensor_msgs::PointCloud2>("voxelcloud",1);
    ros::Publisher pcl_pub3=nh.advertise<sensor_msgs::PointCloud2>("limites",1);
  //  ros::spin();
    cambio=0;
    Cubo.cantidad=0;
    int iis=0;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud2 (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud3 (new pcl::PointCloud<pcl::PointXYZ>);
    sensor_msgs::PointCloud2 output3;
    sensor_msgs::PointCloud2 output2;
    double poseinicialx;
    double poseinicialy;
    double posefinalx=0;
    double posefinaly=0;
    int tamcloud;
    int conteovar1=0,conteovar2=0;
    int movX,movY;
    double diffx,diffy,diffx1,diffy1;
    double auxX,auxY,auxX1,auxY1;
    int auxi=0;
    Carro.cantidad=0;
    Puntos* punto;
    Puntos* puntoss;
    Puntos* puntosvoxel;
    double xvoxel=0.1,yvoxel=0.1,zvozel=0.1;
    int maxX=5,minX=-5,maxY=5,minY=-5,maxZ=1;
    double maxXG[2],minXG[2],maxYG[2],minYG[2];
    maxXG[0]=5,minXG[0]=-5,maxYG[0]=5,minYG[0]=-5;
    double minZ=-1;
    double diffr,diffr1;
    double difftheta;
    int tamano;
	ros::Rate loop_rate(100);
    int alpha=0;
    int alpha1=0;
    int contador=0;
    while(ros::ok())
	{ 
        if(carrito1==1){
            poseinicialx=(posicionx*(1-2*s*pow(qz,2)))+(posiciony*(2*s*qz*qw));
            poseinicialy=(posiciony*(1-2*s*pow(qz,2)))-(posicionx*(2*s*qz*qw));
          //  poseinicialx=posicionx;
          //  poseinicialy=posiciony;
            posefinalx=posicionx;
            posefinaly=posiciony;
            carrito1=2;
        }
        if(carrito==1 && alpha==0 && carrito1==2){
            pcl::fromROSMsg(entrada,*cloud);
            tamcloud=cloud->points.size ();
            punto =new Puntos[tamcloud];
            for (int i=0;i<tamcloud;i++){
                punto[i].x=cloud->points[i].x;
                punto[i].y=cloud->points[i].y;
                punto[i].z=cloud->points[i].z; 
            }
            puntoss =new Puntos[tamcloud];
            puntosvoxel =new Puntos[tamcloud];
            puntoss=limites(punto,tamcloud,maxX,minX,maxY,minY,maxZ,minZ);
            puntosvoxel=organizar(puntoss,puntoss[0].n,xvoxel,yvoxel,zvozel); 
            pcl::PointCloud<pcl::PointXYZ>::Ptr cloud3 (new pcl::PointCloud<pcl::PointXYZ>);
            Carro.movimientocarro[contador][0]=poseinicialx;
            Carro.movimientocarro[contador][1]=poseinicialy;
            if(contador==0){
                tamano=puntosvoxel[0].n;
                cloud3->width=tamano;
                cloud3->height=1;
                cloud3->points.resize (cloud3->width * cloud3->height);
                    for (int i=0; i<tamano;i++){
                            cloud3->points[i].x = puntosvoxel[i].x;
                            cloud3->points[i].y = puntosvoxel[i].y;
                            cloud3->points[i].z = puntosvoxel[i].z;
                            Cubo.valores[Cubo.cantidad][0]=puntosvoxel[i].x;
                            Cubo.valores[Cubo.cantidad][1]=puntosvoxel[i].y;
                            Cubo.valores[Cubo.cantidad][2]=puntosvoxel[i].z;
                            Cubo.cantidad=Cubo.cantidad+1;
                    }     
                std::cout<<tamano<<std::endl;
            }else{
                diffx= (double) (Carro.movimientocarro[contador][0]-Carro.movimientocarro[contador-1][0]);
                diffy= (double) (Carro.movimientocarro[contador][1]-Carro.movimientocarro[contador-1][1]);
                for(int i=0;i<Cubo.cantidad;i++){
                    Cubo.valores[i][0]=Cubo.valores[i][0]-diffx;
                    Cubo.valores[i][1]=Cubo.valores[i][1]-diffy;
                }
                for(int i=0;i<puntosvoxel[0].n;i++){
                    auxi=0;
                    for(int j=0;j<Cubo.cantidad;j++){
                       diffr=sqrt(pow(Cubo.valores[j][0]-puntosvoxel[i].x,2)+pow(Cubo.valores[j][1]-puntosvoxel[i].y,2));
                       diffr1=sqrt(pow(xvoxel,2)+pow(yvoxel,2)+pow(puntosvoxel[i].z,2)); 
                       if(diffr<diffr1 && Cubo.valores[j][2]==puntosvoxel[i].z){
                           auxi=1;
                       }
                    }
                    if(auxi==0){
                        Cubo.valores[Cubo.cantidad][0]=puntosvoxel[i].x;
                        Cubo.valores[Cubo.cantidad][1]=puntosvoxel[i].y;
                        Cubo.valores[Cubo.cantidad][2]=puntosvoxel[i].z;
                        Cubo.cantidad=Cubo.cantidad+1;
                    }
                }
                tamano=Cubo.cantidad;
                cloud3->width=tamano;
                cloud3->height=1;
                cloud3->points.resize (cloud3->width * cloud3->height);
                    for (int i=0; i<tamano;i++){
                            cloud3->points[i].x = Cubo.valores[i][0];
                            cloud3->points[i].y = Cubo.valores[i][1];
                            cloud3->points[i].z = Cubo.valores[i][2];
                    }
            }
            alpha1=alpha1+1;
            tamano=puntoss[0].n;
            cloud2->width=tamano;
            cloud2->height=1;
            cloud2->points.resize (cloud2->width * cloud2->height);
            for (int i=0; i<tamano;i++){
                cloud2->points[i].x = puntoss[i].x;
                cloud2->points[i].y = puntoss[i].y;
                cloud2->points[i].z = puntoss[i].z;
            }
            
            pcl::toROSMsg(*cloud3,output3);
            pcl::toROSMsg(*cloud2,output2);
            output3.header.frame_id =entrada.header.frame_id;
            output2.header.frame_id =entrada.header.frame_id;
            pcl_pub2.publish(output3);
            pcl_pub3.publish(output2);
            alpha=1;
            //pcl::io::savePCDFileASCII ("PruebasCarro4.pcd", *cloud);
            contador++;
        }
        if(sqrt(pow(posefinalx-posicionx,2))>=0.1 || sqrt(pow(posefinaly-posiciony,2))>=0.1 || sqrt(pow(posefinalx-posicionx,2)+pow(posefinaly-posiciony,2))>0.07){
            poseinicialx=((posicionx*(1-2*s*pow(qz,2)))+(posiciony*(2*s*qz*qw)));
            poseinicialy=(posiciony*(1-2*s*pow(qz,2)))-(posicionx*(2*s*qz*qw));
            //poseinicialx=posicionx;
            //poseinicialy=posiciony;
            std::cout<<"Cambio x1:  "<<poseinicialx<<std::endl;
            std::cout<<"Cambio y1:  "<<poseinicialy<<std::endl;
            posefinalx=posicionx;
            posefinaly=posiciony;
            alpha=0;
            std::cout<<"Cambio "<<std::endl; 
         }            
		ros::spinOnce();
		loop_rate.sleep();
	}   
	return 0;
}

