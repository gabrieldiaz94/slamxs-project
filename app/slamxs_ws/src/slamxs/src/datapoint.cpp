#include <iostream>
#include <ros/ros.h>
#include <string>   
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl/io/pcd_io.h>
#include <fstream>
#include <math.h>
#include "slamxs/PointCloudSlamxs.h"

int main(int argc, char **argv)
{
	ros::init (argc, argv,"voxelcubo");
	ros::NodeHandle nh;
    ros::Publisher pcl_pub=nh.advertise<sensor_msgs::PointCloud2>("limitcloud",1);
    ros::Publisher pcl_pub1=nh.advertise<sensor_msgs::PointCloud2>("originalcloud",1);
    ros::Publisher pcl_pub2=nh.advertise<sensor_msgs::PointCloud2>("firstCuadrant",1);
    ros::Publisher pcl_pub3=nh.advertise<sensor_msgs::PointCloud2>("secondCuadrant",1);
    ros::Publisher pcl_pub4=nh.advertise<sensor_msgs::PointCloud2>("thirdCuadrant",1);
    ros::Publisher pcl_pub5=nh.advertise<sensor_msgs::PointCloud2>("fourCuadrant",1);
    system("pwd");

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud2 (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud3 (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud4 (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud5 (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud6 (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PCDReader reader;
    sensor_msgs::PointCloud2 output,output2,output3,output4,output5,output6;
    reader.read<pcl::PointXYZ> ("PruebasLIDAR.pcd",*cloud);

    slamxs::PointCloudSlamxs pointCloudBase;
    for(auto points : cloud->points) {
        pointCloudBase.setPoint(slamxs::SimplePoint{points.x, points.y, points.z});
    }
    std::cout<<"PointCLoudSize: "<<pointCloudBase.pointcloud().size()<<std::endl;
    pointCloudBase.limitPoints();
    std::cout<<"PointCLoudSize: "<<pointCloudBase.pointcloud().empty()<<std::endl;
    std::cout<<"PointCLoudSizeFilter: "<<pointCloudBase.pointcloud().size()<<std::endl;
    cloud2->width = pointCloudBase.pointcloud().size();
    cloud2->height=1;
    cloud2->points.resize (cloud2->width * cloud2->height);
    int it=0;
    for( auto point : pointCloudBase.pointcloud()) {
        cloud2 ->points[it].x = point.x();
        cloud2 ->points[it].y = point.y();
        cloud2 ->points[it].z = point.z();
        it++;
    }

    std::vector<slamxs::SimplePoint> p1 = pointCloudBase.pointsOrganized(1);
    std::vector<slamxs::SimplePoint> p2 = pointCloudBase.pointsOrganized(2);
    std::vector<slamxs::SimplePoint> p3 = pointCloudBase.pointsOrganized(3);
    std::vector<slamxs::SimplePoint> p4 = pointCloudBase.pointsOrganized(4);

    cloud3->width = p1.size();
    cloud3->height=1;
    cloud3->points.resize (cloud3->width * cloud3->height);

    cloud4->width = p2.size();
    cloud4->height=1;
    cloud4->points.resize (cloud4->width * cloud4->height);

    cloud5->width = p3.size();
    cloud5->height=1;
    cloud5->points.resize (cloud5->width * cloud5->height);

    cloud6->width = p4.size();
    cloud6->height=1;
    cloud6->points.resize (cloud6->width * cloud6->height);

    it=0;
    for(auto point : p1) {
        cloud3 ->points[it].x = point.x();
        cloud3 ->points[it].y = point.y();
        cloud3 ->points[it].z = point.z();
        it++;
    }

    it=0;
    for(auto point : p2) {
        cloud4 ->points[it].x = point.x();
        cloud4 ->points[it].y = point.y();
        cloud4 ->points[it].z = point.z();
        it++;
    }

    it=0;
    for(auto point : p3) {
        cloud5 ->points[it].x = point.x();
        cloud5 ->points[it].y = point.y();
        cloud5 ->points[it].z = point.z();
        it++;
    }

    it=0;
    for(auto point : p4) {
        cloud6 ->points[it].x = point.x();
        cloud6 ->points[it].y = point.y();
        cloud6 ->points[it].z = point.z();
        it++;
    }

    std::cout<<cloud2 ->points[it-1].x<<std::endl;
    std::cout<<cloud2 ->points[it-1].y<<std::endl;
    std::cout<<cloud2 ->points[it-1].z<<std::endl;
    ros::Rate loop_rate(100);
    while(ros::ok()) {
        pcl::toROSMsg(*cloud,output);
        pcl::toROSMsg(*cloud2,output2);
        pcl::toROSMsg(*cloud3,output3);
        pcl::toROSMsg(*cloud4,output4);
        pcl::toROSMsg(*cloud5,output5);
        pcl::toROSMsg(*cloud6,output6);
        output.header.frame_id= "map";
        output2.header.frame_id= "map";
        output3.header.frame_id= "map";
        output4.header.frame_id= "map";
        output5.header.frame_id= "map";
        output6.header.frame_id= "map";
        pcl_pub.publish(output2);
        pcl_pub1.publish(output);
        pcl_pub2.publish(output3);
        pcl_pub3.publish(output4);
        pcl_pub4.publish(output5);
        pcl_pub5.publish(output6);

        ros::spinOnce();
		loop_rate.sleep();
    }
    return 0;
}
