#include "slamxs/simplePoint.h"

namespace slamxs{

    SimplePoint::SimplePoint(const double x, const double y, const double z) :
    x_{x}, y_{y}, z_{z} { }; 
    
    SimplePoint::SimplePoint(std::initializer_list<double> list) {
        auto it = list.begin();
        x_ = *it++;
        y_ = *it++;
        z_ = *it;
    }

    bool SimplePoint::operator<=(const SimplePoint &rhs) const {
        return x_ <= rhs.x_ && y_ <= rhs.y_ && z_ <= rhs.z_;
    }

    bool SimplePoint::operator>(const SimplePoint &rhs) const {
        return x_ > rhs.x_ && y_ > rhs.y_ && z_ > rhs.z_;
    }

    bool SimplePoint::operator==(const SimplePoint &rhs) const {
        return x_ == rhs.x_ && y_ == rhs.y_ && z_ == rhs.z_;
    }

    double &SimplePoint::x() { return x_;}
    double &SimplePoint::y() { return y_;}
    double &SimplePoint::z() { return z_;}
}