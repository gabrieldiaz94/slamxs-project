#include "slamxs/PointCloudSlamxs.h"
#include <algorithm>

struct LimitClass {
    double maxX=0.5,minX=-0.5,maxY=0.5,minY=-0.5,maxZ=0.5, minZ=-0.5;
    bool operator() (const slamxs::SimplePoint& value) {
        return !(value <= slamxs::SimplePoint{maxX, maxY, maxZ} && value > slamxs::SimplePoint{minX, minY, minZ});
    }
};

namespace slamxs {
    
    PointCloudSlamxs::PointCloudSlamxs(const double x, const double y, const double z) {
        _pointcloud.push_back(SimplePoint{x, y, z});
    }

    PointCloudSlamxs::PointCloudSlamxs(const SimplePoint &point) {
        _pointcloud.push_back(point);
    }

    void PointCloudSlamxs::setPoint(const SimplePoint& point) {
        _pointcloud.push_back(point);
    }

    void PointCloudSlamxs::limitPoints() {
        _pointcloud.erase(std::remove_if(_pointcloud.begin(), _pointcloud.end(), LimitClass()), _pointcloud.end());
    }

    std::vector<SimplePoint> PointCloudSlamxs::pointsOrganized(int indexCuadrant) {
        std::vector<SimplePoint> firstCuadrant(_pointcloud.size());
        std::vector<SimplePoint> secondCuadrant(_pointcloud.size());
        std::vector<SimplePoint> thirdCuadrant(_pointcloud.size());
        std::vector<SimplePoint> fourCuadrant(_pointcloud.size());

        splitByCuadrant(firstCuadrant, secondCuadrant, thirdCuadrant, fourCuadrant);

        switch (indexCuadrant)
        {
        case 1:
            return firstCuadrant;
        case 2:
            return secondCuadrant;
        case 3:
            return thirdCuadrant;
        case 4:
            return fourCuadrant;
        default:
            return _pointcloud;
        }
    }

    void PointCloudSlamxs::splitByCuadrant(std::vector<SimplePoint> &firstCuadrant, std::vector<SimplePoint> &secondCuadrant, std::vector<SimplePoint> &thirdCuadrant, std::vector<SimplePoint> &fourCuadrant) {
        auto it = std::copy_if(_pointcloud.begin(), _pointcloud.end(), firstCuadrant.begin(), [](SimplePoint &value){return (value.x() < 0 && value.y() >= 0);});
        firstCuadrant.resize(std::distance(firstCuadrant.begin(),it));
        
        it = std::copy_if(_pointcloud.begin(), _pointcloud.end(), secondCuadrant.begin(), [](SimplePoint &value){return (value.x() >= 0 && value.y() >= 0);});
        secondCuadrant.resize(std::distance(secondCuadrant.begin(),it));

        it = std::copy_if(_pointcloud.begin(), _pointcloud.end(), thirdCuadrant.begin(), [](SimplePoint &value){return (value.x() < 0 && value.y() < 0);});
        thirdCuadrant.resize(std::distance(thirdCuadrant.begin(),it));

        it = std::copy_if(_pointcloud.begin(), _pointcloud.end(), fourCuadrant.begin(), [](SimplePoint &value){return (value.x() >= 0 && value.y() < 0);});
        fourCuadrant.resize(std::distance(fourCuadrant.begin(),it));
    }

    std::vector<SimplePoint> &PointCloudSlamxs::pointcloud() {return _pointcloud;}
}